const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('./models/User');

const saltRound = 10; //para criptar la contraseña

const validate = email => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

passport.serializeUser((user, done) => {
    return done(null, user._id);
});

passport.deserializeUser(async(userId, done) => {
    try {
        const existingUser = await User.findById(userId)
        return done(null, existingUser );
    } catch (error) {
        return done(error);
    }
})

passport.use(
    'register', 
    new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async (req, email, password, done) => {
            //logica registro
          try {
            const validEmail = validate(email);
            if(!validEmail) {
                const error = new Error('Invalid email');
                return done(error);
            }

            const previousUser = await User.findOne({email: email.toLowerCase()});
            
            if(previousUser){
                const error = new Error('The user already exists');
                return done(error);
            }
            const hash = await bcrypt.hash(password, saltRound);

            const newUser = new User({
                email: email.toLowerCase(),
                password: hash,
            });

            const savedUser = await newUser.save();

            return done(null, savedUser);
          } catch (error) {
              return done(error);
          }
        }
    )
);

passport.use(
    'login',
    new LocalStrategy (
        {
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async (req, email, password, done) => {
            try {
                const currentUser = await User.findOne({email: email.toLowerCase()});

                if(!currentUser) {
                    const error = new Error('The user does not exists');
                    return done(error);
                }


                const isValidPassword = await bcrypt.compare(
                    password,
                    currentUser.password
                );

                if(!isValidPassword) {
                    const error = new Error('The email or password is invalid!');
                    return done(error);
                }

                return done(null, currentUser);

            } catch (error) {
                return done(error);
            }
        }
    )
    );


   