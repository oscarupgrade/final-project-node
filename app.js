
const express = require('express');
const path = require('path'); //llamamos a path para que configure con hbs las vistas de la web
const passport = require('passport');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
require('./db.js');
require('./passport');
require('dotenv').config();


//variable de Rutas
const indexRouter = require('./routes/index.routes');
const userRouter = require('./routes/user.routes');
const brandsRouter = require('./routes/brands.routes');
const productsRouter = require('./routes/products.routes');
const buyRouter = require('./routes/buy.routes');
const comentsRouter = require('./routes/coments.routes');


const PORT = process.env.Port || 5000;
const app = express();

//Vistas del servidor
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');


// Convierte el body de la petición y lo añade al objeto request
app.use(express.json());
// Convierte el body y lo añade al objeto request
app.use(express.urlencoded({ extended: true }));

// Le decimos a nuestro servidor, qué carpeta será pública.
app.use(express.static(path.join(__dirname, 'public')));



app.use(
    session({
        secret: process.env.SESSION_SECRET,        //yougamer_node
        resave: false,
        saveUninitialized: false,
        cookie: {
            maxAge: 36000000,
        },
        store: new MongoStore({mongooseConnection: mongoose.connection})
    })
)

app.use(passport.initialize());
app.use(passport.session());


//llamada a la ruta con el nombre de la variable arriba creada
app.use('/', indexRouter);
app.use('/users', userRouter);
app.use('/brands', brandsRouter);
app.use('/products', productsRouter);
app.use('/buy', buyRouter);
app.use('/coments', comentsRouter);



//middleware gestión de errores
app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
});


app.use((err, req, res, next) => {
    console.log(err);
    return res.status(err.status || 500).render('error', {
        message: err.message || 'Unexpected error',
        status: err.status || 500
    });
})


app.listen(PORT, () => {
    console.log(`Listening in http://localhost:${PORT}`);
})