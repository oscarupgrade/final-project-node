const express = require('express');
const indexController = require('../controllers/index.controller');
const authMiddleware = require('../middlewares/auth.middleware');

const router = express.Router();

router.get('/', [authMiddleware.isAuthenticated], indexController.indexGet );
router.get('/register', indexController.registerGet);
router.get('/login', indexController.loginGet );



module.exports = router; 