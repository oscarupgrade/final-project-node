const express = require('express');
// const passport = require('passport');
const indexController = require('../controllers/user.controller');
const authMiddleware = require('../middlewares/auth.middleware');
const router = express.Router();

router.get('/', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], indexController.usersGet);

router.post('/register', indexController.registerGet);

router.post('/login', indexController.loginGet );

router.post('/logout', indexController.logoutGet)


module.exports = router;