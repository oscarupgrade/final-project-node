const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const comentsController = require('../controllers/coments.controller');
const router = express.Router();


router.get('/createComent', [authMiddleware.isAuthenticated] ,comentsController.createGet );

router.post('/createComent', [authMiddleware.isAuthenticated] ,comentsController.createPost);

router.get('/showComents', [authMiddleware.isAuthenticated] ,comentsController.showComents);

module.exports = router; 