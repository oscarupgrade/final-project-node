const express = require('express');
const productsController = require('../controllers/products.controller');
const authMiddleware = require('../middlewares/auth.middleware');
const fileMiddleware = require('../middlewares/file.middleware');
const router = express.Router();


// router.get('/detail', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], productsController.indexGet);

router.get('/create', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], productsController.createGet );

router.post('/create', [authMiddleware.isAuthenticated, authMiddleware.isAdmin, fileMiddleware.upload.single("image"), fileMiddleware.uploadToCloudinary ], productsController.createPost ); //fileMiddleware.uploadToCloudinary 

router.put('/edit', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], productsController.editPut);

router.get('/:id/delete', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], productsController.deleteGet);

router.get('/:id', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], productsController.byIdGet);

router.get('/:brand/:periferico', [authMiddleware.isAuthenticated], productsController.productsGet);


module.exports = router;