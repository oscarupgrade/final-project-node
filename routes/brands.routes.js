const express = require('express');
const authMiddleware = require('../middlewares/auth.middleware');
const brandsController = require('../controllers/brands.controller');
const router = express.Router();



router.post('/create', [authMiddleware.isAuthenticated, authMiddleware.isAdmin],  brandsController.createPost );

router.put('/addproducts', brandsController.addPut);  //[authMiddleware.isAuthenticated, authMiddleware.isAdmin]

router.get('/products', [authMiddleware.isAuthenticated, authMiddleware.isAdmin], brandsController.productsGet );

// rutas logitech razer msi corsair

router.get('/logitech', [authMiddleware.isAuthenticated], brandsController.logitechGet );

router.get('/razer', [authMiddleware.isAuthenticated], brandsController.razerGet );

router.get('/msi', [authMiddleware.isAuthenticated], brandsController.msiGet);

router.get('/corsair', [authMiddleware.isAuthenticated], brandsController.corsairGet);




module.exports = router;