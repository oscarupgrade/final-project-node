const express = require('express');
const User = require('../models/User');
// const Products = require('../models/Products');
// const Buy = require('../models/Buy');
const router = express.Router();


router.get('/order-products', async (req, res, next) => { 
 
    try{
    const users = await User.findById(req.user._id).populate("products");
    return res.status(200).render('orders', {users});
    
    }catch(error) {
    next(error);
    }
   });
    
   router.post('/order-products', async (req, res, next) => {
    
    try {
    const { productId } = req.body;
    const userId = req.user._id;
    
    const addBuy = await User.findByIdAndUpdate(
    userId,
    { $addToSet: { products: productId } }, 
    { new: true }
    );
    
    return res.status(200).redirect('/buy/order-products'); 
    
    } catch (error) {
    next(error);
    }
   });


   router.get('/:id/delete', async (req, res, next) => {
    const id = req.params.id;
    try {
        await User.findByIdAndDelete(id);
        res.redirect("/buy/order-products");;

    } catch (error) {
        return next(error);
    }
});





module.exports = router; 