const mongoose = require('mongoose');
require('dotenv').config();  

const DB_URL = 'mongodb://localhost:27017/final-project-node';   // usar en la nube es `mongodb+srv://${process.env.MONGO_DB_USER}:${process.env.MONGO_DB_PASSWORD}@finalnode.xb5al.mongodb.net/yougamer?retryWrites=true&w=majority`

mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
.then( () => {                //para que salga por consola conectado al servidor
    console.log('Connected to DB');
})
.catch(() => {
    console.log('Error connecting to DB');
})

//exportamos LA URL DE LA BASE DE DATOS PARA USARLA EN OTRO ARCHIVO

module.exports = {DB_URL};