const Coments = require('../models/Coments');

const controller = {};

controller.createGet = (req, res, next) => {
    res.status(200).render("addComent");
  };


  controller.createPost = async (req, res, next) => {      
    try {
        const {
            name,
            text,
            valoration,
        } = req.body;

        const newComent = new Coments ({
            name,
            text,
            valoration,
        });
            
        const createdComent = await newComent.save();
        // console.log(createdComent);
         return res.status(201).redirect("/coments/showComents");     //(`/coments/${createdComent._id}`)
   
    } catch (error) {
        return next(error);
    }
};


controller.showComents = async (req, res, next) => {
    try {
        const view = await Coments.find();
        return res.render("showComents", {view});
    } catch (error) {
        return next(error);
    }
};

module.exports = controller;