
const controller = {};

controller.indexGet = async (req, res, next) => {
    try {
        res.status(200).render('index', {title: 'YOUGAMER', isAdmin: req.user.role === 'admin', isUser: req.user.role === 'user'})
    } catch (error) {
        return next(error)
    }
};

controller.registerGet = (req, res, next) => {
    return res.render('register', {title: 'YOUGAMER Register'});
};


controller.loginGet = (req, res, next) => {
    return res.render('login', {title: 'YOUGAMER Login'});
};



module.exports = controller;