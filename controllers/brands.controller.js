const Brands = require('../models/Brands');
const Products = require('../models/Products');

const controller = {};

controller.createPost = async (req, res, next) => {
    try {
        const {brand} = req.body;

        const newBrand =  new Brands({
            brand,
            products: [],
        })
        const createBrands = await newBrand.save();
        return res.status(201).json(createBrands);

    } catch (error) {
        return next(error);
    }
    
};


controller.addPut = async (req, res, next) => {      // probar cuando tenga productos e intentar añadir formulario para añadir productos a marcas hbs add-products
    try {
        const {brandId, productId} = req.body;  // he cambiado el id por el nombre si no funciona volver a cambiar

        const updatedBrands = await Brands.findByIdAndUpdate(brandId,
            {$push: {products: productId }},    //  {$ ne: itemId}
            {new: true}
            );
            return res.status(200).json(updatedBrands);   //he cambiado el json(updatedBrands) por el render si no va volver a cambiar

    } catch (error) {
        return next(error);
    }
};


controller.productsGet = async (req, res, next) => { //añadir middleware admin
    try {
        const products = await Products.find().populate('brands');
        // return res.json(products).pretty;
        return res.render("brands", {products} );
    } catch (error) {
        return next(error);
    }
};




controller.logitechGet = (req, res, next) => {
    return res.render('brands/logitech')
};


controller.razerGet = (req, res, next) => {
    return res.render('brands/razer')
};

controller.msiGet = (req, res, next) => {
    return res.render('brands/msi')
};

controller.corsairGet = (req, res, next) => {
    return res.render('brands/corsair')
};




module.exports = controller;