const Products = require('../models/Products');
const Brands = require('../models/Brands');
const controller = {};


// controller.indexGet = async (req, res, next) =>{
//     try {
//         const products = await Products.find();

//         return res.render('detail-products', {products} ) //hbs products
        
//     } catch (error) {
//         return next(error);
//     }
// };

controller.createGet = (req, res, next) => {
    res.status(200).render("create-product");  //create-products hbs
  };

controller.createPost = async (req, res, next) => {      
    try {
        const {
            tipe,
            name,
            // image,
            description,
            price,
        } = req.body;

        const newProduct = new Products ({
            tipe,
            name,
            image: req.file_url,        // req.file? `/uploads/${req.file.filename}` : null sustituir en caso de que no vaya cloudinary
            description,
            price: Number(price),
        });
            
        const createdProduct = await newProduct.save();
        
        return res.status(201).redirect(`/products/${createdProduct._id}`);
   
    } catch (error) {
        return next(error);
    }
};

controller.editPut = async (req, res, next) => {
    try {
        const {id, price} = req.body;

        const updateProduct = await Products.findByIdAndUpdate(id,
             {price: price},
             {new: true}
        );

        return res.status(200).json(updateProduct); //probar mismo render de creat y añadir boton edit

    } catch (error) {
        return next(error);
    }
};

controller.deleteGet = async (req, res, next) => {
    const id = req.params.id;
    try {
        await Products.findByIdAndDelete(id);
        res.redirect("/brands/products");;

    } catch (error) {
        return next(error);
    }
};

controller.byIdGet = async (req, res, next) => {
    const id = req.params.id;

    try {
        let productFinded = await Products.findById(id);
    
        if(!productFinded) {
            productFinded = false;
        }
    
        return res.status(200).render('detail-product', { title: `${productFinded.name || 'No Product Found'} Page`, product: productFinded, id });
    
      } catch (error) {
        next(error);
      }
    };


    //todas las marcas
    controller.productsGet = async (req, res, next) => {

           const {brand, periferico} = req.params;
            // console.log(req.params);

            try {
                const products = await Brands.find({brand}).populate({
                    path: "products",
                    match: {tipe: periferico}
                });
                
                // return res.json(products);
                return res.render('products/allProducts', {products});

            } catch (error) {
                return next(error);
            }
        
        };

module.exports = controller;