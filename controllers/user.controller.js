const passport = require('passport');
const User = require('../models/User');

const controller = {};

controller.usersGet = async (req, res, next) => {
    try {
        const users = await User.find();

        return res.render('users', {users} )
        
    } catch (error) {
        return next(error);
    }
};

controller.registerGet = (req, res, next) => {
    passport.authenticate('register', (error, user) => {
        if(error) {
            return res.render('register', {error: error.message});
        }
        req.login(user, (error) => {
            if(error) {
                return res.render('register', {error: error.message});
            }
             return res.redirect('/login');  
        });

    })(req, res, next);
};

controller.loginGet = (req, res, next) => {
    passport.authenticate('login', (error, user) => {
        if(error) {
            return res.render('login', {error: error.message});
        }

        req.login(user, (error) => {
            if(error) {
                return res.render('login', {error: error.message});
            }
            return res.redirect('/')
        });

       
    })(req, res, next);
};

controller.logoutGet = (req, res ,next) => {
    if(req.user) {
        req.logOut();
   
    req.session.destroy(() => {
        res.clearCookie('Connect.sid');

        res.redirect('/login');
    })
 } else {
     return res.sendStatus(304);
 }
}


module.exports = controller;