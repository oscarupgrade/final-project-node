Proyecto realizado con NodeJS, en el que he utilizado expressJS y base de datos MongoDB. 
Se trata de una tienda que vende periféricos de ordenador. Tiene tanto para usar como administrador como or un usuario.
En la parte usuario se puede ver los diferentes productos, añadir al carrito de la compra y visualizar el detalle de ese producto.
También se puede añadir comentarios sobre ese producto.
El proyecto está realizado solo en nodeJS usando hbs para las views.
