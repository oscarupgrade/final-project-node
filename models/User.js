const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema (
    {
     email: {type: String, required: true},
     password: {type: String, required: true},
     role: {
         enum: ["admin", "user"],
         type: String,
         default: "user",
         required: true,
     },

     products: [{
        type: mongoose.Types.ObjectId, 
        ref: 'Products',//nombre que se pone segun el nombre de la base de datos
       
    }],
    },
    {timestamps: true,}
);

const User = mongoose.model('Users', userSchema); //'Users' es el nombre que dara a la coleccion

module.exports= User;