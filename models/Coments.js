const mongoose = require('mongoose');

const Schema  = mongoose.Schema;

const comentsSchema = new Schema(

    {   

        name:{
            type: String,
            required: true
        },

        text: {
            type: String,
            required: true
        },

        valoration: {
            enum: ['muy malo', 'malo', 'bueno', 'muy bueno'],
            type: String,
            required: true
        }

    },
    {
        timestamps: true
    }

);


const Coments = mongoose.model('Coments', comentsSchema);
module.exports = Coments;

