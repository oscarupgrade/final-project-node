const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const buySchema = new Schema(
    {

        products: [{
            products: {type: mongoose.Types.ObjectId, 
            ref: 'Products'},//nombre que se pone segun el nombre de la base de datos
           
        }],
    

        users: [{
            type: mongoose.Types.ObjectId,
            ref: 'Users'
        }]
    },
    {
        timestamps: true,
    }
);

const Buy = mongoose.model('Buys', buySchema);

module.exports = Buy;

