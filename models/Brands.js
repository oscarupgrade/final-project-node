const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const brandsSchema = new Schema(
    {
        brand: {
            enum: ['logitech', 'razer', 'msi', 'corsair'],
            type: String,
            required: true
        },

        public_name: {
            type: String,
            required: true
        },

        products: [{
            type: mongoose.Types.ObjectId,
            ref: 'Products'
        }]
    },
    {
        timestamps: true
    },
);

const Brands = mongoose.model('Brands', brandsSchema);

module.exports = Brands;
