const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productsSchema = new Schema(
    {
        tipe: {
            enum: ['keyboard', 'mouse', 'headset'],
            type: String,
            required: true
        },

        name: {
            type: String,
            required: true,
        },

        image: {
            type: String,
            
        },

        description: {
            type: String,
             required: true
        },

        price: {
            type: Number,
            required: true
        },

        public_name: {
            type: String
        }
    },
    {timestamps: true},
);

const Products = mongoose.model('Products', productsSchema); //'Products' es el nombre que se le da a la base de datos

module.exports = Products;