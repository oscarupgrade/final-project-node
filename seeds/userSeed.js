// const mongoose = require('mongoose'); 
// const DB_URL = require('../db.js'); 
// const User = require('../models/User.js'); 

// const users = [
//     {
//         email: "oscar@hotmail.com",
//         password: "1234"
//     },
// ];

// mongoose.connect(DB_URL, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true,
// })
// .then(async () => {
//     const allUsers = await User.find();

//     if(allUsers.length) {                //si tiene contenido vaciamos con el collection.drop lo borramos
//         await User.collection.drop();    // await hace que cuando llegue ahí espera a que esta funcioon se ejcute antes de pasar al siguiente then, es posible porque hacemos la funcion async
//     }
// })
// .catch((err) =>{
//     console.log(`Error deleting db data ${err}`);   //si tiene un error nos imprimirá ese mensaje 
// })

// .then(async () => {
//     await User.insertMany(users);  //insertamos los datos del array users
// })
// .catch ((err) => {
//     console.log(`Error adding data to our db ${err}`);
// })

// .finally(() => mongoose.disconnect());