const multer = require('multer');
const path = require('path');
require('dotenv').config();
const cloudinary = require('cloudinary').v2;
const fs = require('fs');


const VALID_FILE_TYPES = ['image/png', 'image/jpg', 'image/jpeg']; //variable global porque lo ponemos en mayusculas

const storage = multer.diskStorage({
    
    filename: (req, file, cb) => {
        
        cb(null, `${Date.now()}-${file.originalname}`);
    },
    
    destination: (req, file, cb) => {
       
        cb(null, path.join(__dirname, '../public/uploads'))
    },
});

const fileFilter = (req, file, cb) => {
    
    if(!VALID_FILE_TYPES.includes(file.mimetype)) {
        const error = new Error('Invalid file type');
        cb(error, false);
    } else {
        cb(null, true);
    }
}

const upload = multer({
    storage,
    fileFilter,
    limits: {
        fieldNameSize: 500,
        fieldSize: 500000000,
      },
    
});

const uploadToCloudinary = async(req, res, next) => {
    if(req.file) {
        const filePath = req.file.path;
        const image = await cloudinary.uploader.upload(filePath);

        await fs.unlinkSync(filePath);

        req.file_url = image.secure_url; //aqui se alojan las img

        return next();
    } else {
        return next();
    }
}


module.exports = {upload,  uploadToCloudinary};  // meter en exports