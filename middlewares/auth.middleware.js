const isAuthenticated = (req, res, next) => {
    if(req.isAuthenticated()) {
       return next();
    } else {
        return res.redirect('/login');
    }
};

const isAdmin = (req, res, next) => {
    if(req.user.role === 'admin') {
        return next();
    } else {
        return res.redirect('/login')
    }   
};

const isUser = (req,res, next) =>{
    if(req.user.role === 'user') {
        return next();}
};

module.exports = {
    isAuthenticated,
    isAdmin,
    isUser,
}